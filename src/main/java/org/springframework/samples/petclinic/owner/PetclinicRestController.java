package org.springframework.samples.petclinic.owner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

/**
 * Created by ekabanov on 22/03/2017.
 */
@RestController
@RequestMapping("/rest/")
public class PetclinicRestController {

    private final OwnerRepository ownerRepository;

    @Autowired
    public PetclinicRestController(OwnerRepository ownerRepository) {
        this.ownerRepository = ownerRepository;
    }

    @RequestMapping("owners")
    public Collection<Owner> findOwnersByLastName(@RequestParam(value = "name", required = false, defaultValue = "") String name) {
        return ownerRepository.findByLastName(name);
    }

    @RequestMapping("owners/{id}")
    public Owner findOwnersById(@PathVariable("id") Integer id) {
        return ownerRepository.findById(id);
    }
}
