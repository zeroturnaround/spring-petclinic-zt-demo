/*
 * Copyright 2002-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.owner;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

@Controller
@SessionAttributes("supplements")
public class SupplementsController {

    @RequestMapping(value = "/supplements", method = RequestMethod.GET)
    public String processFindForm(Map<String, Object> model, RestTemplate restTemplate) {
        Collection<Supplement> supplements = getSupplements(restTemplate);

        model.put("supplements", supplements);
        return "supplements/supplementsList";
    }

    @RequestMapping(value = "/supplements/pass", method = RequestMethod.GET)
    @ResponseBody
    public Collection<Supplement> getSupplements(RestTemplate restTemplate) {
        String host = System.getProperty("supplements.host");
        String spec = "http://" + host + ":8080/supplements/pass";
        System.out.println(spec);
        return Arrays.asList(restTemplate.getForObject(spec, Supplement[].class));
    }

}
